#define MyAppName "UpWorkOutlook"
#define MyAppExeName "UpWorkOutlook.exe"
#define appVersion "UpWorkOutlook Install 0.9.2"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; \
    GroupDescription: "{cm:AdditionalIcons}"; 

[Icons]
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon; \
    AfterInstall: SetElevationBit('{commondesktop}\{#MyAppName}.lnk')

[Setup]
DisableReadyPage=True
AppName=UpWorkOutlook
AppId=UpWorkOutlook
AppVersion={#appVersion}
RestartIfNeededByRun=False
AllowCancelDuringInstall=False
ExtraDiskSpaceRequired=2
DefaultDirName={pf}\UpWorkOutlook
AllowRootDirectory=True
ShowLanguageDialog=no
LanguageDetectionMethod=locale
UsePreviousGroup=True
DisableProgramGroupPage=yes
AppendDefaultGroupName=False
CloseApplications=True
RestartApplications=False
CreateUninstallRegKey=yes
UninstallDisplayName=UpWorkOutlook
UninstallDisplayIcon={uninstallexe}
MergeDuplicateFiles=False
TerminalServicesAware=False
OutputDir=.\
OutputBaseFilename={#appVersion}
Compression=lzma2/fast
DefaultGroupName=UpWorkOutlook
PrivilegesRequired = admin

[Files]
Source: "UpWorkOutlook\bin\Release\ExcelApi.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "UpWorkOutlook\bin\Release\NetOffice.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "UpWorkOutlook\bin\Release\Office.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "UpWorkOutlook\bin\Release\OfficeApi.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "UpWorkOutlook\bin\Release\OutlookApi.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "UpWorkOutlook\bin\Release\UpWorkOutlook.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\Documents\Visual Studio 2017\Projects\SkatilsyaAutoSender\SkatilsyaAutoSender\bin\Release\SkatilsyaAutoSender.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\Documents\Visual Studio 2017\Projects\SkatilsyaAutoSender\SkatilsyaAutoSender\bin\Release\RestSharp.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "master.xlsx"; DestDir: "{app}"; Flags: ignoreversion
Source: "UpWorkOutlook\bin\Release\Newtonsoft.Json.dll"; DestDir: "{app}"; Flags: ignoreversion

[Code]

procedure SetElevationBit(Filename: string);
var
  Buffer: string;
  Stream: TStream;
begin
  Filename := ExpandConstant(Filename);
  Log('Setting elevation bit for ' + Filename);

  Stream := TFileStream.Create(FileName, fmOpenReadWrite);
  try
    Stream.Seek(21, soFromBeginning);
    SetLength(Buffer, 1);
    Stream.ReadBuffer(Buffer, 1);
    Buffer[1] := Chr(Ord(Buffer[1]) or $20);
    Stream.Seek(-1, soFromCurrent);
    Stream.WriteBuffer(Buffer, 1);
  except
    Log('Setting elevation bit failed');
  finally
    Stream.Free;
  end;
end;

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl";