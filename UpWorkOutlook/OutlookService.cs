﻿using System;
using System.Collections.Generic;

using NetOffice;
using Outlook = NetOffice.OutlookApi;
using NetOffice.OutlookApi.Enums;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Threading.Tasks;

namespace UpWorkOutlook
{
    public class OutlookService : NotifyBase, IDisposable
    {
        const int NO_CONNECTION_ERROR = -2147352567;
        const int NO_INSTANCE_ERROR = -2147467259;
        
        Outlook.Application app = null;
        Outlook._NameSpace ns = null;

        IProvider provider;
        ISorter sorter;
        MasterInfo master;
        bool toStop = false;

        public Dictionary<string, Outlook._Store> Accounts { get; private set; }
        public Outlook._Store ActiveAccount { get; private set; } = null;
        public bool IsRunning { get; set; } = false;
        public OutlookService(IProvider provider)
        {
            this.provider = provider;

            Middleware(Init);
        }
        private void Init()
        {
            app = new Outlook.Application();
            ns = app.GetNamespace("MAPI");

            var accounts = ns.Stores;

            Accounts = new Dictionary<string, Outlook._Store>(accounts.Count);

            foreach (var acc in accounts)
            {
                Accounts.Add(acc.DisplayName, acc);
            }
        }

        public void Start(Outlook._Store account)
        {
            provider.Notify += Provider_Notify;

            NotifyNormal("parsing master...");

            if(!provider.GetMaster(out master))
            {
                NotifyError("failed to parse master, service won't start");
                return;
            }


            sorter = new CustomOutlookSorter(master);

            NotifyNormal("master was successfully parsed");

            if (!IsRunning)
            {
                ActiveAccount = account;

                toStop = false;

                NotifyWarning("don't close outlook during the work of the app");
                NotifyNormal("starting service...");

                Middleware(Loop);
            }
        }

        public void Stop()
        {
            toStop = true;
        }

        private async void Loop()
        {
            IsRunning = true;

            NotifyNormal("working...");

            while (!toStop)
            {
                Middleware(SortInbox);

                if (toStop) break;

                await Task.Delay(3000).ConfigureAwait(false); 
            }

            IsRunning = false;
        }

        private void SortInbox()
        {
            if (TryGetInboxFolder(out Outlook.MAPIFolder inboxFolder))
            {
                Outlook._Items items = inboxFolder.Items;

                foreach (COMObject item in items)
                {
                    if(item is Outlook.MailItem mailItem)
                    {
                        MoveItemToFolder(mailItem, sorter.GetEmailDestinationPath(
                            mailItem.SenderEmailAddress,
                            mailItem.ReceivedTime,
                            mailItem.Subject,
                            mailItem.Body));
                    }
                }

                if(items.Count > 0)
                {
                    ns.SendAndReceive(false);
                }
            }
        }
        private void MoveItemToFolder(Outlook.MailItem item, string path)
        {
            Outlook.MAPIFolder folder;

            if (!TryGetFolder(path, out folder))
                folder = CreateFolder(path);

            if(folder is Outlook.MAPIFolder)
            {
                item.Move(folder);
            }
        }
        private bool TryGetFolder(string folderPath, out Outlook.MAPIFolder folder)
        {
            folder = null;
            string backslash = @"\";

            try
            {
                if (TryGetInboxFolder(out Outlook.MAPIFolder f))
                {
                    folder = f;

                    if (folderPath.StartsWith(@"\\"))
                    {
                        folderPath = folderPath.Remove(0, 2);
                    }
                    string[] folders = folderPath.Split(backslash.ToCharArray());

                    for (int i = 0; i <= folders.GetUpperBound(0); i++)
                    {
                        if (folder.TryGetSubfolder(folders[i], out Outlook.MAPIFolder subfolder))
                        {
                            folder = subfolder;
                        }
                        else
                        {
                            folder = null;
                            return false;
                        }
                    }
                    return true;
                }
            }
            catch { }

            return false;
        }

        private Outlook.MAPIFolder CreateFolder(string folderPath)
        {
            string backslash = @"\";

            try
            {
                if (TryGetInboxFolder(out Outlook.MAPIFolder folder))
                {
                    if (folderPath.StartsWith(@"\\"))
                    {
                        folderPath = folderPath.Remove(0, 2);
                    }
                    string[] folders = folderPath.Split(backslash.ToCharArray());

                    for (int i = 0; i <= folders.GetUpperBound(0); ++i)
                    {
                        if (folder.TryGetSubfolder(folders[i], out Outlook.MAPIFolder subfolder))
                        {
                            folder = subfolder;
                        }
                        else
                        {
                            folder = folder.Folders.Add(folders[i]);
                        }
                    }

                    return folder;

                }
            }
            catch {}

            return null;
        }

        private bool TryGetInboxFolder(out Outlook.MAPIFolder folder)
        {
            folder = null;

            try
            {
                if (ActiveAccount.GetDefaultFolder(OlDefaultFolders.olFolderInbox) is Outlook.MAPIFolder f)
                {
                    folder = f;
                    return true;
                }
            }
            catch { }

            return false;
        }

        private void Middleware(Action action)
        {
            try
            {
                action();
            }
            catch (COMException cex) when (cex.HResult == NO_CONNECTION_ERROR)
            {
                Logger.AddToLogs(MethodBase.GetCurrentMethod().ToString(), cex.ToString());
                NotifyError("couldn't connect to outlook");
            }
            catch (COMException cex) when (cex.HResult == NO_INSTANCE_ERROR)
            {
                Stop();
                Logger.AddToLogs(MethodBase.GetCurrentMethod().ToString(), cex.ToString());
                NotifyError("outlook seems to be shut down, restart the app");
            }
            catch (Exception ex)
            {
                Logger.AddToLogs(MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                NotifyError("[outlook service] something went wrong... check logs.txt");
            }
        }


        private void Provider_Notify(object sender, NotifyArgs e)
        {
            OnNotify(sender, e);
        }


        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Stop();
                    ns?.Dispose();

                    if (app.ActiveWindow() == null)
                    {
                        app?.Quit();
                    }

                    app?.Dispose();
                }

                app = null;
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
