﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace UpWorkOutlook
{
    class Program
    {
        static Dictionary<MessageStatus, string> Prefixes = new Dictionary<MessageStatus, string>()
        {
            { MessageStatus.Normal, string.Empty },
            { MessageStatus.Warning, "WARNING: " },
            { MessageStatus.Error, "ERR: "},
        };

        static OutlookService service;
        const string hklmDir = "OutlookService";

        static void Main(string[] args)
        {
            DateTime trialEnd = new DateTime(2021, 7, 1);

            if(DateTime.Now > trialEnd)
            {
                NotificateErrorAndQuit("trial expired");
            }

            Authenticator auth = new Authenticator();
            auth.Notify += OnNotify;

            if(!auth.CheckActivation())
            {
                NotificateErrorAndQuit("not activated");
            }

            MainLoop();
        }
        private static void MainLoop()
        {
            string startupPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            try
            {
                Echo("starting outlook app...");

                using (service = new OutlookService(new ExcelProvider(
                    Path.Combine(startupPath, "master.xlsx"),
                    Path.Combine(startupPath, "keywords.txt"))))
                {
                    service.Notify += OnNotify;

                    Echo("outlook app started");

                    int count = service.Accounts.Count;

                    if (count > 0)
                    {
                        var accounts = service.Accounts.Keys.ToList();
                        bool loop = true;
                        int num;

                        do
                        {
                            Console.WriteLine();
                            Console.WriteLine("select account:");
                            Console.WriteLine();

                            for (int i = 0; i < count; ++i)
                            {
                                Console.WriteLine($"[{i + 1}] {accounts[i]}");
                            }

                            string input = Console.ReadLine();

                            if (int.TryParse(input, out num) && num > 0 && num <= count)
                            {
                                --num;
                                loop = false;
                            }
                        }
                        while (loop);

                        service.Start(service.Accounts[accounts[num]]);

                        do
                        {
                            Console.Write("press q to stop...");
                        }
                        while (Console.ReadKey().Key != ConsoleKey.Q);

                        service.Stop();
                    }
                    else
                    {
                        Echo("no outlook accounts found to work with", MessageStatus.Warning);
                        Echo("press any key to quit...");
                        Console.ReadKey();
                    }

                    Console.WriteLine();
                    Console.WriteLine("stopping service...");
                }
            }
            catch (Exception ex)
            {
                Logger.AddToLogs(MethodBase.GetCurrentMethod().ToString(), ex.ToString());
                Console.WriteLine("something went wrong... check logs.txt");
            }
        }
        private static void NotificateErrorAndQuit(string message)
        {
            Echo(message, MessageStatus.Error);
            Echo("press any key to quit...");
            Console.ReadKey();
            Environment.Exit(0);
        }
        private static void OnNotify(object sender, NotifyArgs e)
        {
            Echo(e.Message, e.Status, service?.IsRunning ?? false);
        }
        private static void Echo(string message, MessageStatus status = MessageStatus.Normal, bool beforeLastRow = false)
        {
            if(!beforeLastRow)
            {
                Console.WriteLine(Prefixes[status] + message);
                Console.WriteLine();
            }
            else
            {
                int currentTopCursor = Console.CursorTop;
                int currentLeftCursor = Console.CursorLeft;

                Console.MoveBufferArea(0, currentTopCursor, Console.WindowWidth, 1, 0, currentTopCursor + 2);

                Console.CursorTop = currentTopCursor;

                Console.CursorLeft = 0;

                Console.WriteLine(Prefixes[status] + message);
                Console.WriteLine();

                Console.CursorTop = currentTopCursor + 2;
                Console.CursorLeft = currentLeftCursor;
            }
        }
    }
}
