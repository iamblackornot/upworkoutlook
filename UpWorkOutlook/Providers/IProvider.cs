﻿namespace UpWorkOutlook
{
    public interface IProvider : INotify
    {
        bool GetMaster(out MasterInfo info);
    }
}
