﻿using System;

namespace UpWorkOutlook
{
    public class SheetNotFoundException : Exception
    {
        public string SheetName { get; set; }
    }
}
