﻿using System;

namespace UpWorkOutlook
{
    class ColumnNotFound: Exception
    {
        public string ColumnName { get; set; }
    }
}
