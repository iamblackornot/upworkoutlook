﻿using System;

namespace UpWorkOutlook
{
    class ColumnNameNotDefinedException: Exception
    {
        public string ColumnName { get; set; }
    }
}
