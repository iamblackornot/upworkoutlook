﻿using System;

namespace UpWorkOutlook
{
    public class OptionWrongValueException : Exception
    {
        public string OptionName { get; set; }
    }
}