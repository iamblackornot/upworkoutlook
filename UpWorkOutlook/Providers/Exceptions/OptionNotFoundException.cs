﻿using System;

namespace UpWorkOutlook
{
    class OptionNotFoundException : Exception
    {
        public string OptionName { get; set; }
    }
}