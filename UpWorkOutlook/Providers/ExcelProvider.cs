﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NetOffice.ExcelApi.Enums;
using NetOffice.ExcelApi.Tools.Contribution;
using Excel = NetOffice.ExcelApi;
using System.IO;

namespace UpWorkOutlook
{
    public class ExcelProvider: NotifyBase, IProvider
    {
        private string MasterFilePath { get; }
        private string KeywordsFilePath { get; }

        private readonly Dictionary<MasterTable, string> TableNames = new Dictionary<MasterTable, string>()
        {
            { MasterTable.CustomerMaster, "Customer Master" },
            { MasterTable.CompanyMaster, "Company Master" },
            { MasterTable.OptionsMaster, "Options Master" },
        };

        private readonly Dictionary<CustomerColumn, string> CustomerColumnName = new Dictionary<CustomerColumn, string>()
        {
            { CustomerColumn.EMail, "Email id" },
            { CustomerColumn.CompanyName, "Company Name" },
            { CustomerColumn.CompanyNickName, "Company Nick Name" },
            { CustomerColumn.Department, "Department" },
            { CustomerColumn.ProjectName, "Customer Project Name" },
            { CustomerColumn.ProjectCode, "Project Code" },
        };

        private Dictionary<CustomerColumn, int> CustomerColumnIndex;

        private readonly Dictionary<CompanyColumn, string> CompanyColumnName = new Dictionary<CompanyColumn, string>()
        {
            { CompanyColumn.EMail, "Email id" },
        };

        private Dictionary<CompanyColumn, int> CompanyColumnIndex;

        private readonly Dictionary<MasterOption, string> OptionName = new Dictionary<MasterOption, string>()
        {
            { MasterOption.Keywords, "Sorting Keywords" },
            { MasterOption.FYStartDay, "Starting day of FY" },
            { MasterOption.FYStartMonth, "Starting month of FY" },
        };

        public ExcelProvider(string masterFilePath, string keywordsFilePath)
        {
            MasterFilePath = masterFilePath;
            KeywordsFilePath = keywordsFilePath;
        }
        public bool GetMaster(out MasterInfo info)
        {
            info = null;
            Excel.Application excelApp = null;
            Excel.Workbook workBook = null;
            bool res = false;

            try
            {
                excelApp = new Excel.Application();
                excelApp.DisplayAlerts = false;

                CommonUtils utils = new CommonUtils(excelApp);

                if (!File.Exists(MasterFilePath))
                {
                    NotifyError("master spreadsheet not found");
                    return false;
                }

                workBook = excelApp.Workbooks.Open(MasterFilePath, 0, true);

                string sheetName = TableNames[MasterTable.CustomerMaster];
                Excel.Worksheet workSheet = workBook.Worksheets[sheetName] as Excel.Worksheet;

                if (workSheet is null)  throw new SheetNotFoundException() { SheetName = sheetName };

                ParseColumnIndexes(workSheet, CustomerColumnName, out CustomerColumnIndex);
                ParseCustomerMaster(workSheet, out CustomerMaster custMaster);

                sheetName = TableNames[MasterTable.CompanyMaster];
                workSheet = workBook.Worksheets[sheetName] as Excel.Worksheet;

                if (workSheet is null) throw new SheetNotFoundException() { SheetName = sheetName };

                ParseColumnIndexes(workSheet, CompanyColumnName, out CompanyColumnIndex);
                ParseCompanyMaster(workSheet, out CompanyMaster compMaster);

                sheetName = TableNames[MasterTable.OptionsMaster];
                workSheet = workBook.Worksheets[sheetName] as Excel.Worksheet;

                if (workSheet is null) throw new SheetNotFoundException() { SheetName = sheetName };

                ParseOptionsMaster(workSheet, out OptionsMaster om);

                info = new MasterInfo(custMaster, compMaster, om);

                res = true;
            }

            catch (SheetNotFoundException ex)
            {
                NotifyError($"couldn't find \'{ex.SheetName}\' sheet in master");
            }
            catch (ColumnNameNotDefinedException ex)
            {
                NotifyError($"column name not defined [{ex.ColumnName}]");
            }
            catch (ColumnNotFound ex)
            {
                NotifyError($"column \'{ex.ColumnName}\' not found in master");
            }
            catch (OptionNotFoundException ex)
            {
                NotifyError($"option \'{ex.OptionName}\' not found in Options Master");
            }
            catch (OptionWrongValueException ex)
            {
                NotifyError($"option \'{ex.OptionName}\' not set or has inappropriate value in Options Master");
            }
            catch (Exception ex)
            {
                NotifyError("unexpected error occured while parsing \'master\' excel workbook");
                Logger.AddToLogs(MethodBase.GetCurrentMethod().ToString(), ex.ToString());
            }
            finally
            {
                if (workBook != null)
                {
                    workBook?.Close(XlSaveAction.xlDoNotSaveChanges);
                    workBook.Dispose();
                    workBook = null;
                }

                if (excelApp != null)
                {
                    excelApp.Quit();
                    excelApp.Dispose();
                    excelApp = null;
                }
            }

            return res;
        }

        private void ParseCompanyMaster(Excel.Worksheet ws, out CompanyMaster cm)
        {
            cm = null;

            if (ws.UsedRange.Rows.Count > 1)
            {
                HashSet<string> Emails = new HashSet<string>();

                for (int i = 1; i < ws.UsedRange.Rows.Count; ++i)
                {
                    if (ws.Cells[i + 1, CompanyColumnIndex[CompanyColumn.EMail] + 1].TryGetValue(out string email))
                    {
                        if (!Emails.Contains(email))
                        {
                            Emails.Add(email);
                        }
                        else NotifyWarning($"duplicate email [{email}] found in \'Company Master\'. Corresponding entries will be ignored.");
                    }
                }

                cm = new CompanyMaster(Emails);
            }
        }

        private void ParseOptionsMaster(Excel.Worksheet ws, out OptionsMaster om)
        {
            om = null;

            HashSet<string> keywords;
            int FYStartDay;
            int FYStartMonth;

            string temp = GetOptionValue(ws, MasterOption.FYStartDay);

            if (!int.TryParse(temp, out FYStartDay))
                throw new OptionWrongValueException() { OptionName = OptionName[MasterOption.FYStartDay] };

            temp = GetOptionValue(ws, MasterOption.FYStartMonth);

            if (!int.TryParse(temp, out FYStartMonth))
                throw new OptionWrongValueException() { OptionName = OptionName[MasterOption.FYStartMonth] };

            temp = GetOptionValue(ws, MasterOption.Keywords);

            string[] words = temp.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            keywords = new HashSet<string>();

            foreach (string word in words)
                keywords.Add(word.Trim());

            om = new OptionsMaster(keywords, FYStartDay, FYStartMonth);
        }

        private string GetOptionValue(Excel.Worksheet ws, MasterOption option)
        {
            if (ws.TryFindString(OptionName[option], out Excel.Range cell))
            {
                if (ws.Cells[cell.Row, cell.Column + 1].TryGetValue(out object value))
                    return value.ToString(); 
            }

            throw new OptionNotFoundException() { OptionName = OptionName[MasterOption.Keywords] };
        }

        private void ParseCustomerMaster(Excel.Worksheet ws, out CustomerMaster cm)
        {
            cm = null;

            if (ws.UsedRange.Rows.Count > 1)
            {
                Dictionary<string, CustomerRecord> CustomerMaster = new Dictionary<string, CustomerRecord>();
                HashSet<string> Companies = new HashSet<string>();
                Dictionary<string, string> NickNameToCompany = new Dictionary<string, string>();

                for(int i = 1; i < ws.UsedRange.Rows.Count; ++i)
                {
                    ws.Cells[i + 1, CustomerColumnIndex[CustomerColumn.CompanyName] + 1].TryGetValue(out string companyName);
                    ws.Cells[i + 1, CustomerColumnIndex[CustomerColumn.CompanyNickName] + 1].TryGetValue(out string companyNickName);
                    ws.Cells[i + 1, CustomerColumnIndex[CustomerColumn.Department] + 1].TryGetValue(out string department);
                    ws.Cells[i + 1, CustomerColumnIndex[CustomerColumn.ProjectName] + 1].TryGetValue(out string projectName);
                    ws.Cells[i + 1, CustomerColumnIndex[CustomerColumn.ProjectCode] + 1].TryGetValue(out string projectCode);

                    if(ws.Cells[i + 1, CustomerColumnIndex[CustomerColumn.EMail] + 1].TryGetValue(out string email))
                    {
                        if (!CustomerMaster.ContainsKey(email))
                        {
                            CustomerMaster.Add(email, new CustomerRecord()
                            {
                                EMail = email,
                                CompanyName = companyName,
                                CompanyNickName = companyNickName,
                                Department = department,
                                ProjectName = projectName,
                                ProjectCode = projectCode,
                            });
                        }
                        else NotifyWarning($"duplicate email [{email}, {companyName}] found in \'Customer Master\'. Corresponding entries will be ignored.");
                    }

                    if(!string.IsNullOrWhiteSpace(companyName))
                    {


                        if (Companies.Add(companyName) && !string.IsNullOrWhiteSpace(companyNickName))
                        {
                            NickNameToCompany[companyNickName] = companyName;
                        }
                    }
                }

                cm = new CustomerMaster(CustomerMaster, Companies, NickNameToCompany);
            }
        }

        private void ParseColumnIndexes<T>(Excel.Worksheet ws, Dictionary<T, string> ColumnNames, out Dictionary<T, int> ColumnIndex) where T: Enum
        {
            var enums = Utility.GetEnumValues<T>();
            ColumnIndex = new Dictionary<T, int>(enums.Count<T>());

            foreach(var item in enums)
            {
                if (ColumnNames.TryGetValue(item, out string targetColName))
                {
                    if (ws.TryFindString(targetColName, 0, out int colIndex))
                    {
                        ColumnIndex[item] = colIndex;
                    }
                    else throw new ColumnNotFound() { ColumnName = targetColName };
                }
                else throw new ColumnNameNotDefinedException() { ColumnName = item.ToString() };
            }
        }
    }
}
