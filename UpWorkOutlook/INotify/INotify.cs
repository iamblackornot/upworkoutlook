﻿using System;

namespace UpWorkOutlook
{
    public interface INotify
    {
        event EventHandler<NotifyArgs> Notify;
    }
}
