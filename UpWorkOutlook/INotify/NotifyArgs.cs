﻿using System;

namespace UpWorkOutlook
{
    public enum MessageStatus
    {
        Normal = 0,
        Warning = 1,
        Error = 2,
    }
    public class NotifyArgs : EventArgs
    {
        public string Message { get; set; }
        public MessageStatus Status { get; set; } = MessageStatus.Normal;
    }
}
