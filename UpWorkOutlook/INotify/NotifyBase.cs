﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpWorkOutlook
{
    public class NotifyBase : INotify
    {
        public event EventHandler<NotifyArgs> Notify;
        protected void NotifyNormal(string message)
        {
            Notify?.Invoke(this, new NotifyArgs() { Message = message, Status = MessageStatus.Normal });
        }
        protected void NotifyError(string message)
        {
            Notify?.Invoke(this, new NotifyArgs() { Message = message, Status = MessageStatus.Error });
        }
        protected void NotifyWarning(string message)
        {
            Notify?.Invoke(this, new NotifyArgs() { Message = message, Status = MessageStatus.Warning });
        }
        protected void OnNotify(object sender, NotifyArgs e)
        {
            Notify?.Invoke(sender, e);
        }
    }
}
