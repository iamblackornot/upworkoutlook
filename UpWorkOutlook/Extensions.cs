﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Excel = NetOffice.ExcelApi;
using Outlook = NetOffice.OutlookApi;

namespace UpWorkOutlook
{
    public static class Extensions
    {
        public static bool Compare(this string str1, string str2)
        {
            if (!string.IsNullOrWhiteSpace(str1) && !string.IsNullOrWhiteSpace(str2))
            {
                if (str1.Simplify() == str2.Simplify())     return true;
            }

            return false;
        }
        public static string Simplify(this string str)
        {
            return str.RemoveSpecialCharacters().ToLower();
        }
        public static bool Contains(this string source, string value)
        {
            return (source.Simplify().IndexOf(value.Simplify()) > 0);
        }
        public static bool Contains(this string source, IEnumerable<string> values)
        {
            string simplifiedSource = source.Simplify();

            foreach (string value in values)
            {
                if (simplifiedSource.IndexOf(value.Simplify()) > -1)
                    return true;
            }

            return false;
        }
        public static bool Contains(this string source, IEnumerable<string> values, out string match)
        {
            string simplifiedSource = source.Simplify();

            foreach (string value in values)
            {
                if (simplifiedSource.IndexOf(value.Simplify()) > 0)
                {
                    match = value;
                    return true;
                }
            }

            match = null;
            return false;
        }
        public static string RemoveSpecialCharacters(this string str, string replaceString = "")
        {
            return Regex.Replace(str, "[^\\w\\s]+", replaceString);
        }
        public static bool TryFindString(this Excel.Worksheet ws, string searchText, int rowIndex, out int columnIndex)
        {
            columnIndex = -1;

            if (rowIndex > -1 && rowIndex < ws.UsedRange.Rows.Count)
            {
                for (int i = 0; i < ws.UsedRange.Columns.Count; ++i)
                {
                    if (ws.Cells[rowIndex + 1, i + 1].TryGetValue(out object value) && value.ToString().Compare(searchText))
                    //if(ws.Cells[i + 1, rowIndex + 1].TryGetValue(out object value) && value.ToString().Compare(searchText))
                    {
                        columnIndex = i;
                        return true;
                    }
                }
            }

            return false;
        }
        public static bool TryFindString(this Excel.Worksheet ws, string searchText, out Excel.Range cell)
        {
            //for (int row = 0; row < ws.UsedRange.Rows.Count; ++row)
            //{
            //    for (int col = 0; col < ws.UsedRange.Columns.Count; ++col)
            //    {
            //        if (ws.Cells[row + 1, col + 1].TryGetValue(out object value) && value.ToString().Compare(searchText))
            //        {
            //            cell = ws.Cells[row + 1, col + 1];
            //            return true;
            //        }
            //    }
            //}
            if (ws.UsedRange.Find(searchText) is Excel.Range range)
            {
                cell = range;
                return true;
            }

            cell = null;
            return false;
        }
        public static bool TryGetValue(this Excel.Range range, out object value)
        {
            if (!(bool)range.MergeCells)
            {
                if (range.Value is null == false)
                {
                    value = range.Value;
                    return true;
                }
            }
            else
            {
                if (range.MergeArea.Value is object[,] array)
                {
                    foreach (object item in array)
                    {
                        if (item is string val)
                        {
                            value = val;
                            return true;
                        }
                    }
                }
            }

            value = null;
            return false;
        }
        public static bool TryGetValue<T>(this Excel.Range range, out T value)
        {
            if(range.TryGetValue(out object val) && val is T typedVal)
            {
                value = typedVal;
                return true;
            }

            value = default;
            return false;
        }
        public static bool TryGetSubfolder(this Outlook.MAPIFolder parentfolder, string folderName, out Outlook.MAPIFolder folder)
        {
            folder = null;

            if(!string.IsNullOrWhiteSpace(folderName) && parentfolder is Outlook.MAPIFolder)
            {
                foreach(Outlook.MAPIFolder f in parentfolder.Folders)
                {
                    if(f.Name == folderName)
                    {
                        folder = f;
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
