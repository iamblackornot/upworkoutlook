﻿using System;
using System.Collections.Generic;
namespace UpWorkOutlook
{
    public class OptionsMaster
    {
        public HashSet<string> Keywords { get; }
        public int FYStartDay { get; }
        public int FYStartMonth { get; }
        public OptionsMaster(HashSet<string> Keywords, int FYStartDay, int FYStartMonth)
        {
            this.Keywords = Keywords;
            this.FYStartDay = FYStartDay;
            this.FYStartMonth = FYStartMonth;
        }
    }
    enum MasterOption
    {
        Keywords,
        FYStartDay,
        FYStartMonth,
    }
}
