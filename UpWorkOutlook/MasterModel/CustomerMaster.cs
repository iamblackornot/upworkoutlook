﻿using System.Collections.Generic;

namespace UpWorkOutlook
{
    public class CustomerMaster
    {
        public Dictionary<string, CustomerRecord> _customerMaster;// { get; private set; }
        public HashSet<string> Companies { get; private set; }
        public Dictionary<string, string> NickNameToCompany { get; private set; }

        public CustomerMaster(Dictionary<string, CustomerRecord> CustomerMaster,
            HashSet<string> Companies, Dictionary<string, string> NickNameToCompany)
        {
            _customerMaster = CustomerMaster;
            this.Companies = Companies;
            this.NickNameToCompany = NickNameToCompany;
        }
        public bool TryGetRecord(string email, out CustomerRecord record)
        {
            if (_customerMaster.ContainsKey(email))
            {
                record = _customerMaster[email];
                return true;
            }

            record = null;
            return false;
        }
    }
}