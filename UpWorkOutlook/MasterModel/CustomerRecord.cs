﻿namespace UpWorkOutlook
{
    public class CustomerRecord
    {
        public string EMail { get; set; }
        public string CompanyName { get; set; }
        public string CompanyNickName { get; set; }
        public string Department { get; set; }
        public string ProjectName { get; set; }
        public string ProjectCode { get; set; }
    }
    enum CustomerColumn
    {
        EMail,
        CompanyName,
        CompanyNickName,
        Department,
        ProjectName,
        ProjectCode,
    }
}
