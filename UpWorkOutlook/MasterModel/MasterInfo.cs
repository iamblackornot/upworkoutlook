﻿namespace UpWorkOutlook
{
    public class MasterInfo
    {
        public CustomerMaster CustomerMaster { get; }
        public CompanyMaster CompanyMaster { get; }
        public OptionsMaster OptionsMaster { get; }

        public MasterInfo(CustomerMaster custMaster, CompanyMaster compMaster, OptionsMaster om)
        {
            CustomerMaster = custMaster;
            CompanyMaster = compMaster;
            OptionsMaster = om;
        }
    }

    enum MasterTable
    {
        CustomerMaster,
        CompanyMaster,
        OptionsMaster,
    }
}
