﻿using System;

namespace UpWorkOutlook
{
    public interface ISorter
    {
        string GetEmailDestinationPath(string email, DateTime timestamp, string subject, string body);
    }
}
