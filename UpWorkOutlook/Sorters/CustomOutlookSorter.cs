﻿using System;
using System.Collections.Generic;

namespace UpWorkOutlook
{
    public class CustomOutlookSorter : ISorter
    {
        MasterInfo master;

        public CustomOutlookSorter(MasterInfo mi) 
        {
            master = mi;
        }
        public string GetEmailDestinationPath(string email, DateTime timestamp, string subject, string body)
        {
            List<string> folders = new List<string>();

            if (!master.CompanyMaster.Emails.Contains(email))
            {
                if (master.CustomerMaster.TryGetRecord(email, out CustomerRecord cr))
                {
                    if (!string.IsNullOrWhiteSpace(cr.CompanyName) &&
                        !string.IsNullOrWhiteSpace(cr.ProjectName) &&
                        !string.IsNullOrWhiteSpace(cr.Department))
                    {
                        folders.Add("Customer");
                        folders.Add(cr.CompanyName);
                        folders.Add(Utility.DateTimeToFY(timestamp, 
                            master.OptionsMaster.FYStartDay, 
                            master.OptionsMaster.FYStartMonth).ToString());
                        folders.Add(cr.ProjectName);
                        folders.Add(cr.Department);
                    }
                    else
                        folders.Add("General");
                }
                else //email not found in master
                {
                    SortBySubjectAndBody(subject, body, ref folders);
                }
            }
            else //email was forwarded by company's staff
            {
                SortBySubjectAndBody(subject, body, ref folders);
            }

            return string.Join(@"\", folders);
        }
        public void SortBySubjectAndBody(string subject, string body, ref List<string> folders)
        {
            string company = null;

            if (!body.Contains(master.CustomerMaster.Companies, out company))
            {
                if (body.Contains(master.CustomerMaster.NickNameToCompany.Keys, out string nickname))
                {
                    company = master.CustomerMaster.NickNameToCompany[nickname];
                }
            }

            if (subject.Contains(master.OptionsMaster.Keywords))
            {
                if (!string.IsNullOrWhiteSpace(company))
                {
                    folders.Add("Customer");
                    folders.Add(company);
                }

                folders.Add("New Enquiry");
            }
            else // no keywords found in subject 
            {
                if (!string.IsNullOrWhiteSpace(company))
                {
                    folders.Add("Customer");
                    folders.Add(company);
                }

                folders.Add("General");
            }
        }
    }
}
