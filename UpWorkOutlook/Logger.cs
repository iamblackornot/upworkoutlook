﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.IO;

namespace UpWorkOutlook
{
    public static class Logger
    {
        private static readonly object lcLogs = new Object();
        public static void AddToLogs(string location, string message)
        {
            Task.Factory.StartNew(delegate
            {
                lock (lcLogs)
                {
                    File.AppendAllText(Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "logs.txt"), 
                        DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + " " + location + " : " + message + Environment.NewLine);
                }
            }, CancellationToken.None, TaskCreationOptions.PreferFairness, TaskScheduler.Default);
        }
        public static void AddToLogs(string message)
        {
            AddToLogs(string.Empty, message);
        }
    }
}

