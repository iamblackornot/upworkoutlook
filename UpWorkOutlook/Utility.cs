﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpWorkOutlook
{
    public static class Utility
    {
        public static IEnumerable<T> GetEnumValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }
        public static int DateTimeToFY(DateTime dt, int FYStartDay, int FYStartMonth)
        {
            return dt.Month >= FYStartMonth && dt.Day >= FYStartDay ? dt.Year - 1 : dt.Year;
        }
        public static double TimeNow()
        {
            TimeSpan epochTicks = new TimeSpan(new DateTime(1970, 1, 1).Ticks);
            TimeSpan unixTicks = new TimeSpan(DateTime.UtcNow.Ticks) - epochTicks;
            return Math.Floor(unixTicks.TotalSeconds);
        }
    }
}
