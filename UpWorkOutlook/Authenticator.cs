﻿using System;
using RestSharp;
using System.Dynamic;
using Newtonsoft.Json;
using Microsoft.Win32;

namespace UpWorkOutlook
{
    public class Authenticator : NotifyBase
    {
        public bool CheckActivation()
        {
            if (!TryGetMachineGuid(out string guid))
            {
                NotifyError("failed to retrieve device id...");
                return false;
            }

            NotifyNormal("connecting to authentication server...");

            bool res = false;

            try
            {
                var client = new RestClient("https://verificationtest.herokuapp.com/auth");
                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("accept", "application/json");

                dynamic json = new ExpandoObject();
                json.guid = guid;

                request.AddParameter("application/json", JsonConvert.SerializeObject(json), ParameterType.RequestBody);
                var response = client.Execute(request);

                if (!response.IsSuccessful)
                {
                    NotifyError(response.ErrorMessage);
                    return false;
                }

                string decrypted = Cryptography.Decrypt(guid, response.Content);
                bool activated = bool.Parse(decrypted.Substring(guid.Length));

                if (!activated)
                {
                    NotifyNormal("application is not activated for your pc.");
                    NotifyNormal($"your device id: {guid}");
                }

                res = activated;
            }
            catch(Exception ex)
            {
                Logger.AddToLogs(ex.ToString());
                NotifyError("failed to check activation, check logs.txt");
            }

            return res;
        }
        private bool TryGetMachineGuid(out string guid)
        {
            guid = null;
            bool res = false;

            try
            {
                using (RegistryKey key = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Cryptography"))
                {
                    if (key != null)
                    {
                        object o = key.GetValue("MachineGuid");

                        if (o != null)
                        {
                            guid = o.ToString();
                            res = true;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Logger.AddToLogs(ex.ToString());
            }

            return res;
        }
    }
}
